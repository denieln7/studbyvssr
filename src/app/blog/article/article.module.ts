import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ArticleComponent } from "./article.component";
import { AppRoutingModule } from "src/app/app-routing.module";

@NgModule({
  declarations: [ArticleComponent],
  imports: [CommonModule, AppRoutingModule],
  exports: [ArticleComponent]
})
export class ArticleModule {}
