import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-article",
  templateUrl: "./article.component.html",
  styleUrls: ["./article.component.scss"]
})
export class ArticleComponent implements OnInit {
  post = null;
  articleId = null;
  link = null;

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this.articleId = this.route.snapshot.paramMap.get("id");
    this.http
      .get(`/assets/articles/${this.articleId}.json`)
      .subscribe(response => {
        this.post = response["html"];
        this.link = response["link"];
      });
  }

  ngOnInit() {}
}
