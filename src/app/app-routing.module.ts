import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ErrorPageComponent } from "./error-page/error-page.component";
import { BlogComponent } from "./blog/blog.component";
import { GalleryComponent } from "./gallery/gallery.component";
import { ArticleComponent } from "./blog/article/article.component";
import { MainPageComponent } from "./main-page/main-page.component";
import { CommercialComponent } from "./main-page/projects/commercial/commercial.component";
import { ResidentialComponent } from "./main-page/projects/residential/residential.component";
import { PredajComponent } from "./products/predaj/predaj.component";
import { RealizaciaComponent } from "./products/realizacia/realizacia.component";
import { PoradenstvoComponent } from "./products/poradenstvo/poradenstvo.component";
import { CaparolComponent } from "./products/caparol/caparol.component";

const appRoutes: Routes = [
  {
    path: "",
    component: MainPageComponent,
    children: [
      { path: "", redirectTo: "komercne", pathMatch: "full" },
      { path: "komercne", component: CommercialComponent },
      { path: "rezidencne", component: ResidentialComponent }
    ]
  },
  {
    path: "produkt/predaj",
    component: PredajComponent
  },
  {
    path: "produkt/realizacia",
    component: RealizaciaComponent
  },
  {
    path: "produkt/poradenstvo",
    component: PoradenstvoComponent
  },
  {
    path: "produkt/caparol",
    component: CaparolComponent
  },
  {
    path: "blog",
    component: BlogComponent
  },
  {
    path: "blog/article/:id",
    component: ArticleComponent
  },
  {
    path: "gallery",
    component: GalleryComponent
  },
  {
    path: "gallery/:tag",
    component: GalleryComponent
  },
  {
    path: "not-found",
    component: ErrorPageComponent,
    data: { message: "This Page is Lost in Space" }
  },
  { path: "**", redirectTo: "not-found" }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
