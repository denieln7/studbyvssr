import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-predaj",
  templateUrl: "./predaj.component.html",
  styleUrls: ["./predaj.component.scss"]
})
export class PredajComponent implements OnInit {
  // @Output() tag: EventEmitter<string> = new EventEmitter<string>();
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {}

  // showInspirationGallery() {
  //   this.router.navigate(["/gallery", { tag: "walls", active: 4 }]);
  // }
}
