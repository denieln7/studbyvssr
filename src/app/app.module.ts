import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ErrorPageComponent } from "./error-page/error-page.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BlogComponent } from "./blog/blog.component";
import { GalleryComponent } from "./gallery/gallery.component";
import { GalleryModule } from "@ks89/angular-modal-gallery";
import { ArticleModule } from "./blog/article/article.module";
import { TopContactHeaderComponent } from "./top-contact-header/top-contact-header.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MainPageModule } from "./main-page/main-page.module";
import { PredajComponent } from "./products/predaj/predaj.component";
import { RealizaciaComponent } from "./products/realizacia/realizacia.component";
import { PoradenstvoComponent } from "./products/poradenstvo/poradenstvo.component";
import { CaparolComponent } from "./products/caparol/caparol.component";
import { HttpClientModule } from "@angular/common/http";
import { EventsService } from "./services/events.service";
import { TopMenuModule } from './top-menu/top-menu.module';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [
    AppComponent,
    TopContactHeaderComponent,
    ErrorPageComponent,
    BlogComponent,
    GalleryComponent,
    PredajComponent,
    RealizaciaComponent,
    PoradenstvoComponent,
    CaparolComponent
  ],
  imports: [
    BrowserModule.withServerTransition({
      appId: "studiobyvania-ag"
    }),
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    TopMenuModule,
    GalleryModule.forRoot(),
    HttpClientModule,
    ArticleModule,
    MainPageModule,
    ClickOutsideModule
  ],
  providers: [EventsService],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule {}
