import { Component, AfterViewInit } from "@angular/core";
import { EventsService } from "./services/events.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements AfterViewInit {
  title = "studbyvssr";
  menuOpen = false;
  currentSection = 'section1';

  constructor(private events: EventsService) {
    //ak sa klikne na menu ikonku tak sa dostane sem
    this.events.subscribe((v: any) => {
      this.menuOpen = v;
    });
  }

  resetMenuIcon() {
    this.menuOpen = !this.menuOpen;
    this.events.emit(false);//send false value to change side menu icon
  }

  scrollTo(section) {
    this.resetMenuIcon();

    document.querySelector('#' + section)
      .scrollIntoView({ behavior: 'smooth', block: 'center' });
  }

  onClickedOutsideOfSideMenu() {
    if(this.menuOpen) {
      this.resetMenuIcon();
    }
  }

  ngAfterViewInit() {
  }

  ngOnInit() {}
}
