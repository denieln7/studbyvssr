import { Component, OnInit } from "@angular/core";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import { EventsService } from "../services/events.service";

@Component({
  selector: "app-top-menu",
  templateUrl: "./top-menu.component.html",
  styleUrls: ["./top-menu.component.scss"]
})
export class TopMenuComponent implements OnInit {
  public isCollapsed = true;
  faCircle = faCircle;
  status = false;

  constructor(private events: EventsService) {
    //sem sa dostane ak sa zatvorí bočné menu keď kliknem na nejakú sekciu, mení sa menu ikonka
    this.events.subscribe((v: any) => {
      this.status = v;
    });
  }

  ngOnInit() {}

  toggleMenu() {
    this.status = !this.status;
    this.events.emit(this.status);
  }
}
