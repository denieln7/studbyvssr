import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { TopMenuComponent } from "./top-menu.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { AppRoutingModule } from "../app-routing.module";
// import { MainPageModule } from "../main-page/main-page.module";

@NgModule({
  declarations: [TopMenuComponent],
  imports: [BrowserModule, NgbModule, FontAwesomeModule, AppRoutingModule],
  providers: [],
  exports: [TopMenuComponent],
  bootstrap: []
})
export class TopMenuModule {}
