import { Component, OnInit } from '@angular/core';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';


@Component({
  selector: 'app-top-contact-header',
  templateUrl: './top-contact-header.component.html',
  styleUrls: ['./top-contact-header.component.scss']
})
export class TopContactHeaderComponent implements OnInit {
  faPhoneAlt = faPhoneAlt;
  faEnvelope = faEnvelope;
  faFacebookF = faFacebookF;
  faInstagram = faInstagram;

  constructor() { }

  ngOnInit() {
  }

}
