import { Component, AfterViewInit, ViewChild, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements AfterViewInit {
  @Input("titles") titles: Array<any> = [];
  @Input("intervals") intervals: Array<number> = [];
  @Input("src") src: String;
  @Input("title") title: String;
  @Input("direction") direction: String = "left";

  @ViewChild("video", { static: false }) video: ElementRef;

  private canPlayThroughVideo = false;
  private contentVisible = false;

    
  
  ngAfterViewInit() {
    this.video.nativeElement.addEventListener("canplaythrough", () => {
      this.canPlayThroughVideo = true;
      if (this.contentVisible) {
        this.playVideo(this.video);
      }
    })
  }  

  visibleOnScreen() {
    this.contentVisible = true;

    if (this.canPlayThroughVideo) {
      this.playVideo(this.video);
    }

    this.video.nativeElement.addEventListener("play", () => {
      this.nextTitle(this.titles, this.intervals, 0);
    })
  }

  playVideo(el) {
    const media = el.nativeElement;
    media.muted = true;
    media.play();
  }

  nextTitle(titles, intervals, index) {
    setTimeout(() => {
      titles[index].active = true;

      if (intervals.length > index + 1) {
        this.nextTitle(titles, intervals, ++index);
      }
    }, intervals[index] * 1000);
  }
}
