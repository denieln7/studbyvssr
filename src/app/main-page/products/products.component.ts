import { Component, ViewChild } from '@angular/core';
import { ProductComponent } from './product/product.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {
  @ViewChild("exterior", { static: false }) exterior: ProductComponent;
  @ViewChild("interior", { static: false }) interior: ProductComponent;

  exteriorTitles: Array<any> = [
    { title: "Fasádne farby a omietky,", active: false },
    { title: "dekoračné fasádne nátery,", active: false },
    { title: "pohľadový betón,", active: false },
    { title: "nátery na drevo , kovy a betón,", active: false },
    { title: "obkladový kameň a tehly,", active: false },
    { title: "dekoprofily,", active: false },
    { title: "kompletné zatepľovacie systémy a špeciálne nátery", active: false },
  ]

  exteriorIntervals: Array<number> = [
    2.1, 1, 1.55, 1.25, 1.25, 1.8, 1
  ]

  interiorTitles: Array<any> = [
    { title: "Interiérové farby,", active: false },
    { title: "tapety a fototapety,", active: false },
    { title: "pohľadový betón a dekoračné omietky ,", active: false },
    { title: "mozaiky a obkladový kameň,", active: false },
    {
      title: "bytový textil, tieniace a závesné systémy,", active: false
    },
    { title: "bytové doplnky a svietidlá,", active: false },
    { title: "podlahy, koberce a dekoprofily", active: false },
  ]

  interiorIntervals: Array<number> = [
    1.0, 1.5, 0.8, 1.7, 1.2, 1.3, 1.0
  ]

  constructor() {
  }

  visibleOnScreen() {
    this.exterior.visibleOnScreen();
    this.interior.visibleOnScreen();
  }
}
