import { Component, OnInit, Inject, PLATFORM_ID } from "@angular/core";
// import { Slick } from "ngx-slickjs";
import { DomSanitizer } from "@angular/platform-browser";
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: "app-main-slider",
  templateUrl: "./main-slider.component.html",
  styleUrls: ["./main-slider.component.scss"]
})
export class MainSliderComponent implements OnInit {
  constructor(private domSanitizer: DomSanitizer, @Inject(PLATFORM_ID) private platformId: Object) {}
  public currentSlide: number = 0;

  // images = [
  //   "https://picsum.photos/id/76/450/487",
  //   "https://picsum.photos/id/77/450/487",
  //   "https://picsum.photos/id/78/450/487"
  // ];

  isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  ngOnInit() {}

  // config: Slick.Config = {
  //   infinite: true,
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   dots: true,
  //   arrows: false,
  //   autoplay: false,
  //   autoplaySpeed: 2000,
  //   mouseWheelMove: false,

  //   // the magic
  //   // responsive: [
  //   //   {
  //   //     breakpoint: 992,
  //   //     settings: {
  //   //       slidesToShow: 2
  //   //     }
  //   //   },
  //   //   {
  //   //     breakpoint: 768,
  //   //     settings: {
  //   //       slidesToShow: 1
  //   //     }
  //   //   },
  //   //   {
  //   //     breakpoint: 576,
  //   //     settings: {
  //   //       slidesToShow: 1,
  //   //       arrows: false
  //   //     }
  //   //   }
  //   // ]
  // };

  slideConfig = { "slidesToShow": 1, "slidesToScroll": 1, "arrows": false, "dots": true, "infinite": true, "autoplay": false, "autoplaySpeed": 2000 };


  afterChange({ currentSlide }) {
    this.currentSlide = currentSlide;
  }

  // imageUrl(url) {
  //   return this.domSanitizer.bypassSecurityTrustStyle(`url("${url}")`);
  // }
}
