import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainSliderComponent } from "./main-slider.component";
// import { NgxSlickJsModule } from "ngx-slickjs";
import { SlickModule } from 'ngx-slick';

@NgModule({
  declarations: [MainSliderComponent],
  imports: [
    CommonModule, 
    // NgxSlickJsModule.forRoot(),
    SlickModule.forRoot()],
  exports: [MainSliderComponent]
})
export class MainSliderModule {}
