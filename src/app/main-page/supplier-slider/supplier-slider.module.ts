import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SupplierSliderComponent } from "./supplier-slider.component";
// import { NgxSlickJsModule } from "ngx-slickjs";
import { SupplierComponent } from './supplier/supplier.component';
import { SlickModule } from 'ngx-slick';


@NgModule({
  declarations: [SupplierSliderComponent, SupplierComponent],
  imports: [
    CommonModule,
    // NgxSlickJsModule.forRoot(),
    SlickModule.forRoot()],
  exports: [SupplierSliderComponent]
})
export class SupplierSliderModule {}
