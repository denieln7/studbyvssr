export class Supplier {
  link: string;
  imagePath: string;

  constructor(link: string, imagePath: string) {
    this.link = link;
    this.imagePath = imagePath;
  }
}
