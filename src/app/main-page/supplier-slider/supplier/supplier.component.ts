import { Component, OnInit, Input } from "@angular/core";
import { Supplier } from "./supplier.model";

@Component({
  selector: "app-supplier",
  templateUrl: "./supplier.component.html",
  styleUrls: ["./supplier.component.scss"]
})
export class SupplierComponent implements OnInit {
  @Input() supplier: Supplier;

  constructor() {}

  ngOnInit() {}
}
