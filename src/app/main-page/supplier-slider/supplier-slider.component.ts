import { Component, OnInit, AfterViewInit, Inject, PLATFORM_ID } from "@angular/core";
// import { Slick } from "ngx-slickjs";
import { Supplier } from "./supplier/supplier.model";
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: "app-supplier-slider",
  templateUrl: "./supplier-slider.component.html",
  styleUrls: ["./supplier-slider.component.scss"]
})
export class SupplierSliderComponent implements OnInit, AfterViewInit {
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  // config: Slick.Config = {
  //   infinite: true,
  //   slidesToShow: 6,
  //   slidesToScroll: 1,
  //   dots: false,
  //   arrows: true,
  //   autoplay: true,
  //   autoplaySpeed: 2000,
  //   mouseWheelMove: false,

  //   // the magic
  //   responsive: [
  //     {
  //       breakpoint: 992,
  //       settings: {
  //         slidesToShow: 3
  //       }
  //     },
  //     {
  //       breakpoint: 768,
  //       settings: {
  //         slidesToShow: 3
  //       }
  //     },
  //     {
  //       breakpoint: 576,
  //       settings: {
  //         slidesToShow: 2,
  //         arrows: false
  //       }
  //     }
  //   ]
  // };

  slideConfig = { 
    "slidesToShow": 6, 
    "slidesToScroll": 1, 
    "arrows": false,

    responsive: [
      {
        breakpoint: 768,
          settings: {
            "slidesToShow": 2
          }
      }
    ]
  
  };

  suppliers: Supplier[] = [
    new Supplier(
      "https://www.raschtextil.de/index.php",
      "../../assets/images/suppliers/rasch.png"
    ),
    new Supplier(
      "https://texamhome.com/en",
      "../../assets/images/suppliers/texam.png"
    ),
    new Supplier(
      "http://www.caselio.fr/",
      "../../assets/images/suppliers/caselio.png"
    ),
    new Supplier(
      "http://www.camengo.fr/",
      "../../assets/images/suppliers/camengo.png"
    ),
    new Supplier(
      "http://www.oikos-group.it/it/s/interni/soluzioni-decorative-per-interni",
      "../../assets/images/suppliers/oikos.png"
    ),
    new Supplier(
      "https://www.aldeco.pt/",
      "../../assets/images/suppliers/aldeco.png"
    ),
    new Supplier(
      "https://www.dizz-design.eu/",
      "../../assets/images/suppliers/dizz.png"
    ),
    new Supplier(
      "http://www.mardomdecor.com/",
      "../../assets/images/suppliers/mardom-dekor.png"
    ),    
    new Supplier(
      "https://www.dunin.eu/sk/",
      "../../assets/images/suppliers/dunin.png"
    ), 
    new Supplier(
      "http://www.atlaswallco.com/",
      "../../assets/images/suppliers/atlas.png"
    ),
    new Supplier(
      "http://www.khroma.be/en/nor",
      "../../assets/images/suppliers/khroma.png"
    ),
    new Supplier(
      "https://www.emilianaparati.com/it/default.asp",
      "../../assets/images/suppliers/emiliana.png"
    ),
    new Supplier(
      "http://www.sirpi-wallcoverings.it/en/",
      "../../assets/images/suppliers/sirpi.png"
    ),    
    new Supplier(
      "https://www.komar.de/",
      "../../assets/images/suppliers/komar.png"
    ),
    new Supplier(
      "https://www.vescom.com/",
      "../../assets/images/suppliers/vescom.png"
    ),
    new Supplier(
      "http://www.casamance.com/",
      "../../assets/images/suppliers/casamance.png"
    ),
    new Supplier(
      "http://www.spiver.it/it/a",
      "../../assets/images/suppliers/spiver.png"
    ),
    new Supplier(
      "http://www.casadeco.fr/",
      "../../assets/images/suppliers/casadeco.png"
    ),
    new Supplier(
      "http://omexco.com/",
      "../../assets/images/suppliers/omexco.png"
    ),
    new Supplier(
      "https://www.oracdecor.com/en/?store_switch=true",
      "../../assets/images/suppliers/orac.png"
    ),
    new Supplier(
      "https://www.marburg.com/en/",
      "../../assets/images/suppliers/marburg.png"
    ),
    new Supplier(
      "https://www.prestigious.co.uk/",
      "../../assets/images/suppliers/pt.png"
    ),
    new Supplier("https://leha.eu/", "../../assets/images/suppliers/leha.png"),
    new Supplier(
      "https://en.kobe.eu/",
      "../../assets/images/suppliers/kobe.png"
    ),
    new Supplier(
      "https://www.arte-international.com/en",
      "../../assets/images/suppliers/arte.png"
    ),
    new Supplier(
      "https://www.hookedonwalls.com/en",
      "../../assets/images/suppliers/hw.png"
    ),
    new Supplier(
      "https://www.mrperswall.com/",
      "../../assets/images/suppliers/mr-perswall.png"
    ),
    new Supplier(
      "http://www.baustyr.sk/",
      "../../assets/images/suppliers/baustyr.png"
    )
  ];

  ngOnInit() {}

  ngAfterViewInit() {
  }
}
