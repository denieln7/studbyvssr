import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GalleryModule } from "@ks89/angular-modal-gallery";
import { ResidentialComponent } from "./residential.component";

@NgModule({
  declarations: [ResidentialComponent],
  imports: [CommonModule, GalleryModule],
  exports: [ResidentialComponent]
})
export class ResidentialModule {}
