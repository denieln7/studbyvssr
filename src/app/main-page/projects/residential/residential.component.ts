import { Component, OnInit } from "@angular/core";
import {
  GalleryService,
  Image,
  PlainGalleryConfig,
  PlainGalleryStrategy,
  LineLayout
} from "@ks89/angular-modal-gallery";

@Component({
  selector: "app-residential",
  templateUrl: "./residential.component.html",
  styleUrls: ["./residential.component.scss"]
})
export class ResidentialComponent implements OnInit {
  constructor(private galleryService: GalleryService) {}

  ngOnInit() {}

  images1: Image[] = [
    new Image(0, {
      img: "https://picsum.photos/id/11/700/394",
      extUrl: "http://www.google.com"
    }),
    new Image(1, {
      img: "https://picsum.photos/id/12/700/394",
      description: "Description 2"
    })
  ];
  images2: Image[] = [
    new Image(0, {
      img: "https://picsum.photos/id/13/700/394",
      extUrl: "http://www.google.com"
    }),
    new Image(1, {
      img: "https://picsum.photos/id/14/700/394",
      description: "Description 2"
    })
  ];
  images3: Image[] = [
    new Image(0, {
      img: "https://picsum.photos/id/15/700/394",
      extUrl: "http://www.google.com"
    }),
    new Image(1, {
      img: "https://picsum.photos/id/16/700/394",
      description: "Description 2"
    })
  ];

  openModalViaService(id: number | undefined, index: number) {
    console.log("opening gallery with index " + index);
    this.galleryService.openGallery(id, index);
  }

  plainGalleryRowATagsStart: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.ROW,
    layout: new LineLayout(
      { width: "50px", height: "50px" },
      { length: 6, wrap: true },
      "flex-start"
    ),
    advanced: { aTags: true, additionalBackground: "50% 50%/cover" }
  };

  plainGalleryRowATagsEnd: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.ROW,
    layout: new LineLayout(
      { width: "50px", height: "50px" },
      { length: 6, wrap: true },
      "flex-end"
    ),
    advanced: { aTags: true, additionalBackground: "50% 50%/cover" }
  };
}
