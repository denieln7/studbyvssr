import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GalleryModule } from "@ks89/angular-modal-gallery";
import { CommercialComponent } from "./commercial.component";

@NgModule({
  declarations: [CommercialComponent],
  imports: [CommonModule, GalleryModule],
  exports: [CommercialComponent]
})
export class CommercialModule {}
