import { Component, OnInit } from "@angular/core";
import {
  GalleryService,
  Image,
  PlainGalleryConfig,
  PlainGalleryStrategy,
  LineLayout
} from "@ks89/angular-modal-gallery";

@Component({
  selector: "app-commercial",
  templateUrl: "./commercial.component.html",
  styleUrls: ["./commercial.component.scss"]
})
export class CommercialComponent implements OnInit {
  public galleryId = 100;
  constructor(private galleryService: GalleryService) {}

  ngOnInit() {}

  komercne: Image[] = [
    new Image(0, {
      img: "../../../../assets/images/projects/hanuliak.jpg",
      extUrl: "http://www.google.com"
    }),
    new Image(1, {
      img: "../../../../assets/images/projects/rozsutec-fasada.jpg",
      description: "Description 2"
    }),
    new Image(2, {
      img: "../../../../assets/images/projects/rozsutec-restauracia.jpg",
      description: "Description 2"
    }),
    new Image(3, {
      img: "../../../../assets/images/projects/chateau-gbelany.jpg",
      description: "Description 2"
    }),
    new Image(4, {
      img: "../../../../assets/images/projects/chateau-gbelany-wellness.jpg",
      description: "Description 2"
    }),
    new Image(5, {
      img: "https://picsum.photos/id/14/700/394",
      description: "Description 2"
    })
  ];
  images2: Image[] = [
    new Image(0, {
      img: "https://picsum.photos/id/15/700/394",
      extUrl: "http://www.google.com"
    }),
    new Image(1, {
      img: "https://picsum.photos/id/16/700/394",
      description: "Description 2"
    }),
    new Image(2, {
      img: "https://picsum.photos/id/17/700/394",
      description: "Description 2"
    }),
    new Image(3, {
      img: "https://picsum.photos/id/18/700/394",
      description: "Description 2"
    }),
    new Image(4, {
      img: "https://picsum.photos/id/19/700/394",
      description: "Description 2"
    }),
    new Image(5, {
      img: "https://picsum.photos/id/20/700/394",
      description: "Description 2"
    })
  ];
  images3: Image[] = [
    new Image(0, {
      img: "https://picsum.photos/id/21/700/394",
      extUrl: "http://www.google.com"
    }),
    new Image(1, {
      img: "https://picsum.photos/id/22/700/394",
      description: "Description 2"
    }),
    new Image(2, {
      img: "https://picsum.photos/id/23/700/394",
      description: "Description 2"
    }),
    new Image(3, {
      img: "https://picsum.photos/id/24/700/394",
      description: "Description 2"
    }),
    new Image(4, {
      img: "https://picsum.photos/id/25/700/394",
      description: "Description 2"
    }),
    new Image(5, {
      img: "https://picsum.photos/id/26/700/394",
      description: "Description 2"
    })
  ];

  openModalViaService(id: number | undefined, index: number) {
    console.log("opening gallery with index " + index);
    this.galleryService.openGallery(id, index);
  }

  plainGalleryRowATagsStart: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.ROW,
    layout: new LineLayout(
      { width: "50px", height: "50px" },
      { length: 4, wrap: true },
      "flex-start"
    ),
    advanced: { aTags: true, additionalBackground: "50% 50%/cover" }
  };

  plainGalleryRowATagsEnd: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.ROW,
    layout: new LineLayout(
      { width: "50px", height: "50px" },
      { length: 4, wrap: true },
      "flex-end"
    ),
    advanced: { aTags: true, additionalBackground: "50% 50%/cover" }
  };
}
