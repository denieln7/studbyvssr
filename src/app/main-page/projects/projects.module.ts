import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProjectsComponent } from "./projects.component";
import { AppRoutingModule } from "src/app/app-routing.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [ProjectsComponent],
  imports: [CommonModule, AppRoutingModule, FontAwesomeModule],
  exports: [ProjectsComponent]
})
export class ProjectsModule {}
