import { Component, OnInit } from "@angular/core";
import { faCircle } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-our-team",
  templateUrl: "./our-team.component.html",
  styleUrls: ["./our-team.component.scss"]
})
export class OurTeamComponent implements OnInit {
  faCircle = faCircle;
  constructor() {}

  ngOnInit() {}
}
