import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
// import { NgxSlickJsModule } from "ngx-slickjs";
import { QuotesSliderComponent } from "./quotes-slider.component";
import { SlickModule } from 'ngx-slick';

@NgModule({
  declarations: [QuotesSliderComponent],
  imports: [
    CommonModule, 
    // NgxSlickJsModule.forRoot(), 
    SlickModule.forRoot()
  ],
  exports: [QuotesSliderComponent]
})
export class QuotesSliderModule {}
