import { Component, OnInit, Inject, PLATFORM_ID } from "@angular/core";
// import { Slick } from "ngx-slickjs";
import { isPlatformBrowser } from "@angular/common";

@Component({
  selector: "app-quotes-slider",
  templateUrl: "./quotes-slider.component.html",
  styleUrls: ["./quotes-slider.component.scss"]
})
export class QuotesSliderComponent implements OnInit {
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  ngOnInit() {}
  // config: Slick.Config = {
  //   infinite: true,
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   dots: true,
  //   arrows: false,
  //   autoplay: true,
  //   autoplaySpeed: 2000,
  //   mouseWheelMove: false,

  //   // the magic
  //   responsive: [
  //     {
  //       breakpoint: 992,
  //       settings: {
  //         slidesToShow: 1
  //       }
  //     },
  //     {
  //       breakpoint: 768,
  //       settings: {
  //         slidesToShow: 1
  //       }
  //     },
  //     {
  //       breakpoint: 576,
  //       settings: {
  //         slidesToShow: 1,
  //         arrows: false
  //       }
  //     }
  //   ]
  // };
  
  slideConfig = { "slidesToShow": 1, "slidesToScroll": 1, "arrows": false, "dots": true, "infinite": true, "autoplay": true, "autoplaySpeed": 2000 };
}
