import { Component, OnInit } from "@angular/core";
import { FormGroup, NgForm } from "@angular/forms";
import { faInstagram, faFacebookF } from "@fortawesome/free-brands-svg-icons";

@Component({
  selector: "app-contact-us",
  templateUrl: "./contact-us.component.html",
  styleUrls: ["./contact-us.component.scss"]
})
export class ContactUsComponent implements OnInit {
  faInstagram = faInstagram;
  faFacebookF = faFacebookF;

  contactForm = FormGroup;
  emailToSend = {
    name: "",
    email: "",
    number: "",
    message: ""
  };

  constructor() {}

  ngOnInit() {}

  onSubmit(form: NgForm) {
    this.emailToSend.name = form.value.userData.name;
    this.emailToSend.email = form.value.userData.email;
    this.emailToSend.number = form.value.userData.number;
    this.emailToSend.message = form.value.userData.message;

    form.reset();
  }
}
