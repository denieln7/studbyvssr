import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ContactUsComponent } from "./contact-us.component";
import { FormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [ContactUsComponent],
  imports: [CommonModule, FormsModule, FontAwesomeModule],
  exports: [ContactUsComponent]
})
export class ContactUsModule {}
