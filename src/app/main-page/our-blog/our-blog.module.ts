import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OurBlogComponent } from "./our-blog.component";
import { AppRoutingModule } from "src/app/app-routing.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [OurBlogComponent],
  imports: [CommonModule, AppRoutingModule, FontAwesomeModule],
  exports: [OurBlogComponent]
})
export class OurBlogModule {}
