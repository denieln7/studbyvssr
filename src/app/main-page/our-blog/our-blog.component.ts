import { Component, OnInit } from "@angular/core";
import { faCircle } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-our-blog",
  templateUrl: "./our-blog.component.html",
  styleUrls: ["./our-blog.component.scss"]
})
export class OurBlogComponent implements OnInit {
  faCircle = faCircle;

  constructor() {}

  ngOnInit() {}
}
