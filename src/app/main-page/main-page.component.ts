import { Component, OnInit, ViewChild } from "@angular/core";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { ProductsComponent } from './products/products.component';
import { faInstagram, faFacebookF } from "@fortawesome/free-brands-svg-icons";


@Component({
  selector: "app-main-page",
  templateUrl: "./main-page.component.html",
  styleUrls: ["./main-page.component.scss"]
})
export class MainPageComponent implements OnInit {
  faInstagram = faInstagram;
  faFacebookF = faFacebookF;
  faChevronRight = faChevronRight;
  @ViewChild("products", { static: false }) products: ProductsComponent;
  constructor() {}

  ngOnInit() {
    var observer = new IntersectionObserver((entries) => {
      // isIntersecting is true when element and viewport are overlapping
      // isIntersecting is false when element and viewport don't overlap
      if (entries[0].isIntersecting === true) {
        // this.works.visibleOnScreen();
        this.products.visibleOnScreen();
        console.log('Element has just become visible in screen');
      }
    }, { threshold: [0.3] });

    observer.observe(document.querySelector("#section2"));
  }

  ngAfterViewInit() {
    
  }

  onSectionChange($event) {
    console.log("on section change v main" + $event);
  }
}
