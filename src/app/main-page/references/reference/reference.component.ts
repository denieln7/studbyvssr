import { Component, OnInit, Input } from "@angular/core";
import { Reference } from "./reference.model";

@Component({
  selector: "app-reference",
  templateUrl: "./reference.component.html",
  styleUrls: ["./reference.component.scss"]
})
export class ReferenceComponent implements OnInit {
  @Input() reference: Reference;

  constructor() {}

  ngOnInit() {}
}
