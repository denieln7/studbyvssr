import { Component, OnInit } from "@angular/core";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
// import { Slick } from "ngx-slickjs";
import { Reference } from "./reference/reference.model";

@Component({
  selector: "app-references",
  templateUrl: "./references.component.html",
  styleUrls: ["./references.component.scss"]
})
export class ReferencesComponent implements OnInit {
  faCircle = faCircle;

  // config: Slick.Config = {
  //   infinite: true,
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   dots: false,
  //   arrows: true,
  //   autoplay: true,
  //   autoplaySpeed: 2000,

  //   // the magic
  //   responsive: [
  //     {
  //       breakpoint: 768,
  //       settings: {
  //         slidesToShow: 2
  //       }
  //     },
  //     {
  //       breakpoint: 576,
  //       settings: {
  //         slidesToShow: 1,
  //         arrows: false
  //       }
  //     }
  //   ]
  // };

  slideConfig = { "slidesToShow": 1, "slidesToScroll": 1 };


  references: Reference[] = [
    new Reference(
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "Mgr. Alena Galová"
    ),
    new Reference(
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "Stanislava Matúšová"
    ),
    new Reference(
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "Martin Kysela"
    ),
    new Reference(
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "Vladimír Brun"
    )
  ];

  constructor() {}

  ngOnInit() {}
}
