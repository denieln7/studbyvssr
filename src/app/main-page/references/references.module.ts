import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReferencesComponent } from "./references.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
// import { NgxSlickJsModule } from "ngx-slickjs";
import { ReferenceComponent } from './reference/reference.component';
import { SlickModule } from 'ngx-slick';


@NgModule({
  declarations: [ReferencesComponent, ReferenceComponent],
  imports: [
    CommonModule, 
    FontAwesomeModule, 
    // NgxSlickJsModule.forRoot(), 
    SlickModule.forRoot()],
  exports: [ReferencesComponent]
})
export class ReferencesModule {}
