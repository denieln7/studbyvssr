import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainPageComponent } from "./main-page.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { OurTeamComponent } from "./our-team/our-team.component";
import { SupplierSliderModule } from "./supplier-slider/supplier-slider.module";
import { ProjectsModule } from "./projects/projects.module";
import { CommercialModule } from "./projects/commercial/commercial.module";
import { ResidentialModule } from "./projects/residential/residential.module";
import { OurBlogModule } from "./our-blog/our-blog.module";
import { AppRoutingModule } from "../app-routing.module";
import { ReferencesModule } from "./references/references.module";
import { ContactUsModule } from "./contact-us/contact-us.module";
import { MainSliderModule } from "./main-slider/main-slider.module";
import { QuotesSliderModule } from "./quotes-slider/quotes-slider.module";
import { ProductsModule } from './products/products.module';

@NgModule({
  declarations: [MainPageComponent, OurTeamComponent],
  imports: [
    CommonModule,
    SupplierSliderModule,
    ProjectsModule,
    CommercialModule,
    FontAwesomeModule,
    ResidentialModule,
    OurBlogModule,
    AppRoutingModule,
    ReferencesModule,
    ContactUsModule,
    MainSliderModule,
    QuotesSliderModule,
    ProductsModule
  ],
  exports: [MainPageComponent]
})
export class MainPageModule {}
