import { Image, ModalImage } from "@ks89/angular-modal-gallery";

export class MyImage extends Image {
  tags: Array<string>;

  constructor(id: number, modal: ModalImage, tags: Array<string>) {
    super(id, modal);
    this.tags = tags;
  }
}
