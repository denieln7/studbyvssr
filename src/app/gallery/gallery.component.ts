import { Component, OnInit } from "@angular/core";
import {
  GalleryService,
  PlainGalleryConfig,
  PlainGalleryStrategy,
  LineLayout
} from "@ks89/angular-modal-gallery";
import { MyImage } from "./my-image.model";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-gallery",
  templateUrl: "./gallery.component.html",
  styleUrls: ["./gallery.component.scss"]
})
export class GalleryComponent implements OnInit {
  allDisplayed = false;
  static LENGTH = 3; // set how many photos display
  offset = 0;
  tag = null;

  constructor(
    private galleryService: GalleryService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loadMore();
    if (this.route.snapshot.params.tag) {
      this.tag = this.route.snapshot.params.tag;
      this.setTag(this.tag);
    }
  }

  imagesToShow: MyImage[] = [];

  myImages: MyImage[] = [
    new MyImage(
      0,
      {
        img: "../../assets/images/gallery/tapety/tapety_1.jpg",
        extUrl: "http://www.google.com"
      },
      ["inspiration", "wallpapers"]
    ),
    new MyImage(
      1,
      {
        img: "../../assets/images/gallery/tapety/tapety_2.jpg",
        description: "Description 2"
      },
      ["inspiration", "wallpapers"]
    ),
    new MyImage(
      2,
      {
        img: "../../assets/images/gallery/tapety/tapety_3.jpg",
        description: "Description 2"
      },
      ["inspiration", "wallpapers"]
    ),
    new MyImage(
      3,
      {
        img: "../../assets/images/gallery/fasada/fasada_1.jpg",
        description: "Description 2"
      },
      ["inspiration", "walls"]
    ),
    new MyImage(
      4,
      {
        img: "../../assets/images/gallery/fasada/fasada_2.jpg",
        description: "Description 2"
      },
      ["inspiration", "walls"]
    ),
    new MyImage(
      5,
      {
        img: "../../assets/images/gallery/fasada/fasada_3.jpg",
        description: "Description 2"
      },
      ["inspiration", "walls"]
    ),
    new MyImage(
      6,
      {
        img: "../../assets/images/gallery/fasada/fasada_4.jpg",
        extUrl: "http://www.google.com"
      },
      ["inspiration", "walls"]
    ),
    new MyImage(
      7,
      {
        img: "../../assets/images/gallery/farby/farba_1.jpg",
        description: "Description 2"
      },
      ["inspiration", "colors"]
    ),
    new MyImage(
      8,
      {
        img: "../../assets/images/gallery/farby/farba_2.jpg",
        description: "Description 2"
      },
      ["inspiration", "colors"]
    ),
    new MyImage(
      9,
      {
        img: "../../assets/images/gallery/farby/farba_3.jpg",
        description: "Description 2"
      },
      ["inspiration", "colors"]
    ),
    new MyImage(
      10,
      {
        img: "../../assets/images/gallery/farby/farba_4.jpg",
        description: "Description 2"
      },
      ["inspiration"]
    ),
    new MyImage(
      11,
      {
        img: "../../assets/images/gallery/tapety/tapety_8.jpg",
        description: "Description 2"
      },
      ["inspiration"]
    )
  ];

  openModalViaService(id: number | undefined, index: number) {
    this.galleryService.openGallery(id, index);
  }

  plainGalleryRowATags: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.ROW,
    layout: new LineLayout(
      { width: "2%", height: "200px" },
      { length: 0, wrap: true },
      "justify-content: start;"
    ),

    advanced: { aTags: true, additionalBackground: "50% 50%/cover" }
  };

  loadMore() {
    let founded = 0;
    const temp = [];

    while (
      founded != GalleryComponent.LENGTH &&
      this.offset < this.myImages.length
    ) {
      if (
        this.myImages[this.offset]["tags"].some(item => item == this.tag) ||
        !this.tag
      ) {
        temp.push(this.myImages[this.offset]);
        founded++;
      }
      this.offset++;
    }
    this.imagesToShow = [...this.imagesToShow, ...temp];
  }

  setTag(tag: string) {
    this.imagesToShow = [];
    this.offset = 0;
    this.tag = tag;
    this.loadMore();
  }
}
