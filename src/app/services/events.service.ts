import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class EventsService {
  private events: Subject<any>;

  constructor() {
    this.events = new Subject<any>();
  }

  subscribe(func: Function) {
    this.events.subscribe({
      next: v => func(v)
    });
  }

  emit(value: any) {
    this.events.next(value);
  }
}
