import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Data } from "@angular/router";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-error-page",
  templateUrl: "./error-page.component.html",
  styleUrls: ["./error-page.component.scss"]
})
export class ErrorPageComponent implements OnInit {
  errorMessage: string;
  faChevronLeft = faChevronLeft;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      this.errorMessage = data["message"];
    });
  }
}
